﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public decimal Precio { get; set; }
        public Boolean Activo { get; set; }
        public string Tipo { get; set; }
        public int Stock { get; set; }
        public List<string> Codigos { get; set; }

     public Product ()
     {
     }



    public Product(int id, string nombre, string descripcion, decimal precio, Boolean activo, string tipo, int stock, List<string> codigos)
        {
            Id = id;
            Nombre = nombre;
            Descripcion = descripcion;
            Precio = precio;
            Activo = activo;
            Tipo = tipo;
            Stock = stock;
            Codigos = codigos;

        }
    }
    
}