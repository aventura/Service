﻿using Service.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Service.Datamodel
{   
    
    public class ProductService
    {
        public static List<Product> listProducts;
        Product producto = new Product();

        public ProductService()
        {
            if(listProducts==null || !listProducts.Any())
            listProducts = GenerarProductos();
        }

        public List<Product> ListarProductos()
        {      
            return listProducts; 
        }
        public Product BuscarProducto(int id)
        {
            var product = listProducts.FirstOrDefault((p) => p.Id == id);

            return product;
        }
        public List<Product> RegistrarProducto(Product producto)
        {
            listProducts.Add(producto);
            return listProducts;
        }
        public List<Product> ModificarProducto(int id, Product product)
        {
            producto = BuscarProducto(id);
            if (producto != null)
            {
                producto = product;
                int pos = id - 1;
                listProducts.Insert(pos, producto);
                listProducts.RemoveAt(id);
            }

            return listProducts;
        }
        public List<Product> EliminarProducto(int id)
        {
            
            producto = BuscarProducto(id);
            if (producto != null){
                int pos = id - 1;
                listProducts.RemoveAt(pos);
            }
            
            return listProducts;
        }

        private List<Product> GenerarProductos()
        {
            List<Product> listaProductos = new List<Product>();
            List<string> codigos = new List<string>();
            codigos.Add("779222");
            codigos.Add("779111");

            Product producto = new Product(1, "laptop Soup", "Groceries", 10, true, "producto", 100, codigos);
            Product producto2 = new Product(2, "huawei Soup", "Groceries", 10, true, "producto", 100, codigos);
            Product producto3 = new Product(3, "monitor Soup", "Groceries", 10, true, "producto", 100, codigos);
            Product producto4 = new Product(4, "teclado Soup", "Groceries", 10, true, "producto", 100, codigos);
            Product producto5 = new Product(5, "phone Soup", "Groceries", 10, true, "producto", 100, codigos);

            listaProductos.Add(producto);
            listaProductos.Add(producto2);
            listaProductos.Add(producto3);
            listaProductos.Add(producto4);
            listaProductos.Add(producto5);

            return (listaProductos);

        }


    }
}


