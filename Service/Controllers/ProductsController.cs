﻿using Service.Datamodel;
using Service.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace ProductsApp.Controllers
{   
    [RoutePrefix("api/product")]
    public class ProductsController : ApiController
    {
        private ProductService productService;

        public ProductsController(){
            productService = new ProductService();
        }

        [HttpGet]
        [ActionName("Products")]
        public IHttpActionResult GetAllProducts()
        {
            return Ok(productService.ListarProductos());
        }

        [HttpGet]
        [ActionName("Products")]
        public IHttpActionResult GetProduct(int id)
        {
            
            Product product = productService.BuscarProducto(id);

            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
            
        }
        [HttpPost]
        [ActionName("Registrar")]
        public IHttpActionResult RegistrarProducto(Product producto)
        {
            return Ok(productService.RegistrarProducto(producto));

        }
        [HttpPut]
        [ActionName("Modificar")]
        public IHttpActionResult ModificarProducto(int id,Product producto)
        {
            return Ok(productService.ModificarProducto(id, producto));

        }
        [HttpDelete]
        [ActionName("Eliminar")]
        public IHttpActionResult EliminarProducto(int id)
        {
            return Ok(productService.EliminarProducto(id));

        }



    }
}
